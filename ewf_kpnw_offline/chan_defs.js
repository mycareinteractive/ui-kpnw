
//////////////////////////////////////////////////////////////////////////////////////////
// Code for building the channel list used by the offline app
//////////////////////////////////////////////////////////////////////////////////////////

// Array of available channels
var Chan_Table = new Array();
	
/*function BuildChanRF(type, freq, program, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "rf";
	chanobj.ID = chn;			          	//channel number
	chanobj.program = program;	          	//program	number
	chanobj.freq = freq;			      	//frequency
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}
*/
function BuildChanRF(freq, program, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "rf";
	chanobj.ID = chn;			          	//channel number
	chanobj.program = program;	          	//program	number
	chanobj.freq = freq;			      	//frequency
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

function BuildChanIP(ip, port, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "ip";
	chanobj.ID = chn;			          	//channel number
	chanobj.ip = ip;						//IP address
	chanobj.port = port						//port
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

var idx = 0;
// IP Example:                     IP                  port        ID      label       usage: A = audio, AV = audio and video
//Chan_Table[idx++] = BuildChanIP("239.79.200.34",  "20034",    "02",   "Travel Channel HD",        "AV");
// RF Example:                    freq	   prog   ID	label  usage: A = audio,  AV = audio and video
//Chan_Table[idx++] = BuildChanRF( "657000", "1", "02", "CNN", "AV"); 
// Analog Example:                freq     nothing   ID    label  usage: A = audio,  AV = audio and video
//Chan_Table[idx++] = BuildChanRF( "657000", "", "02", "CNN", "AV");

Chan_Table[idx++] = BuildChanRF("57000", "", "2", "ABC", "AV");
Chan_Table[idx++] = BuildChanRF("63000", "", "3", "CW", "AV");
Chan_Table[idx++] = BuildChanRF("69000", "", "4", "TV Guide", "AV");
Chan_Table[idx++] = BuildChanRF("79000", "", "5", "PAX", "AV");
Chan_Table[idx++] = BuildChanRF("85000", "", "6", "CBS", "AV");
Chan_Table[idx++] = BuildChanRF("177000", "", "7", "Discovery", "AV");
Chan_Table[idx++] = BuildChanRF("183000", "", "8", "NBC", "AV");
Chan_Table[idx++] = BuildChanRF("189000", "", "9", "WGN (Local Chicago)", "AV");
Chan_Table[idx++] = BuildChanRF("195000", "", "10", "KOPB", "AV");
Chan_Table[idx++] = BuildChanRF("201000", "", "11", "Public Access", "AV");
Chan_Table[idx++] = BuildChanRF("207000", "", "12", "KPTV", "AV");
Chan_Table[idx++] = BuildChanRF("213000", "", "13", "KPDX", "AV");
Chan_Table[idx++] = BuildChanRF("123000", "", "14", "Jewelry", "AV");
Chan_Table[idx++] = BuildChanRF("129000", "", "15", "TV MART", "AV");
Chan_Table[idx++] = BuildChanRF("135000", "", "16", "QVC", "AV");
Chan_Table[idx++] = BuildChanRF("141000", "", "17", "Home Shopping", "AV");
Chan_Table[idx++] = BuildChanRF("147000", "", "18", "Hallmark", "AV");
Chan_Table[idx++] = BuildChanRF("159000", "", "20", "KNMT", "AV");
Chan_Table[idx++] = BuildChanRF("165000", "", "21", "PUBACC 2", "AV");
Chan_Table[idx++] = BuildChanRF("171000", "", "22", "ShopNBC", "AV");
Chan_Table[idx++] = BuildChanRF("220000", "", "23", "PUBACC 3", "AV");
Chan_Table[idx++] = BuildChanRF("225000", "", "24", "CSPAN", "AV");
Chan_Table[idx++] = BuildChanRF("231000", "", "25", "CSPAN2", "AV");
Chan_Table[idx++] = BuildChanRF("237000", "", "26", "Telemundo", "AV");
Chan_Table[idx++] = BuildChanRF("243000", "", "27", "TVCTV 27", "AV");
Chan_Table[idx++] = BuildChanRF("255000", "", "28", "EPG Educational Access", "AV");
Chan_Table[idx++] = BuildChanRF("261000", "", "30", "EPG Government Access", "AV");
Chan_Table[idx++] = BuildChanRF("267000", "", "31", "KUNP", "AV");
Chan_Table[idx++] = BuildChanRF("273000", "", "32", "NBC Sports", "AV");
Chan_Table[idx++] = BuildChanRF("279000", "", "33", "Golf", "AV");
Chan_Table[idx++] = BuildChanRF("285000", "", "34", "ROOT Sports Northwest", "AV");
Chan_Table[idx++] = BuildChanRF("291000", "", "35", "ESPN", "AV");
Chan_Table[idx++] = BuildChanRF("297000", "", "36", "ESPN2", "AV");
Chan_Table[idx++] = BuildChanRF("303000", "", "37", "Comcast SportsNet", "AV");
Chan_Table[idx++] = BuildChanRF("309000", "", "38", "TLC", "AV");
Chan_Table[idx++] = BuildChanRF("315000", "", "39", "ABC Family", "AV");
Chan_Table[idx++] = BuildChanRF("321000", "", "40", "Nickelodeon", "AV");
Chan_Table[idx++] = BuildChanRF("327000", "", "41", "Disney", "AV");
Chan_Table[idx++] = BuildChanRF("333000", "", "42", "Cartoon Network", "AV");
Chan_Table[idx++] = BuildChanRF("339000", "", "43", "Animal Planet", "AV");
Chan_Table[idx++] = BuildChanRF("345000", "", "44", "CNN", "AV");
Chan_Table[idx++] = BuildChanRF("351000", "", "45", "HLN", "AV");
Chan_Table[idx++] = BuildChanRF("357000", "", "46", "CNBC", "AV");
Chan_Table[idx++] = BuildChanRF("363000", "", "47", "Weather", "AV");
Chan_Table[idx++] = BuildChanRF("369000", "", "48", "Fox News", "AV");
Chan_Table[idx++] = BuildChanRF("375000", "", "49", "Northwest Cable News", "AV");
Chan_Table[idx++] = BuildChanRF("381000", "", "50", "History", "AV");
Chan_Table[idx++] = BuildChanRF("387000", "", "51", "truTV", "AV");
Chan_Table[idx++] = BuildChanRF("393000", "", "52", "A & E", "AV");
Chan_Table[idx++] = BuildChanRF("399000", "", "53", "FX", "AV");
Chan_Table[idx++] = BuildChanRF("405000", "", "54", "TNT", "AV");
Chan_Table[idx++] = BuildChanRF("411000", "", "55", "TBS", "AV");
Chan_Table[idx++] = BuildChanRF("417000", "", "56", "BET", "AV");
Chan_Table[idx++] = BuildChanRF("423000", "", "57", "Spike TV", "AV");
Chan_Table[idx++] = BuildChanRF("429000", "", "58", "USA", "AV");
Chan_Table[idx++] = BuildChanRF("435000", "", "59", "Syfy", "AV");
Chan_Table[idx++] = BuildChanRF("441000", "", "60", "Comedy Central", "AV");
Chan_Table[idx++] = BuildChanRF("447000", "", "61", "Country Music Television", "AV");
Chan_Table[idx++] = BuildChanRF("453000", "", "62", "VH1", "AV");
Chan_Table[idx++] = BuildChanRF("459000", "", "63", "MTV - Music Television", "AV");
Chan_Table[idx++] = BuildChanRF("465000", "", "64", "TV Land", "AV");
Chan_Table[idx++] = BuildChanRF("471000", "", "65", "Travel", "AV");
Chan_Table[idx++] = BuildChanRF("477000", "", "66", "Food", "AV");
Chan_Table[idx++] = BuildChanRF("483000", "", "67", "Home & Garden", "AV");
Chan_Table[idx++] = BuildChanRF("489000", "", "68", "Oxygen", "AV");
Chan_Table[idx++] = BuildChanRF("495000", "", "69", "Lifetime", "AV");
Chan_Table[idx++] = BuildChanRF("501000", "", "70", "E! Entertainment", "AV");
Chan_Table[idx++] = BuildChanRF("513000", "", "72", "New Born", "AV");
Chan_Table[idx++] = BuildChanRF("519000", "", "73", "New Born (sp)", "AV");
Chan_Table[idx++] = BuildChanRF("525000", "", "74", "The CARE Channel", "AV");



///////////////////////////////////////////////////////////////////////////////////////////////////
var MaxChans = Chan_Table.length;
var DefChannel = 0;

// Array of available channels30
var ChanList = new Array();

// Build a channel list object
function BuildChannel(Label, ChanDesc) {
	var ChanObj = new Object();
	ChanObj.ChanDesc = ChanDesc;
	ChanObj.Label = Label;
	return ChanObj;
}

// Build Chan_Table list
for (i=0; i < MaxChans; i++) {
    var ch = Chan_Table[i];
	var ChanUsage = "AudioVideo";
	if (ch.audio_video == "A") {
		ChanUsage = "AudioOnly";
	}
	var isDigital = true;
	var prgNum = parseInt(ch.program);
	if (isNaN(prgNum) || prgNum <= 0) {
	    isDigital = false;
	}
	
	if (ch.type == "rf" && isDigital) {
		ChanList.push(BuildChannel('"' + ch.label + '"', 
										'<ChannelParams ChannelType="Digital" \
											ChannelUsage="' + ChanUsage + '"> \
											<DigitalChannelParams \
												PhysicalChannelIDType="Freq" \
												PhysicalChannelID="' + ch.freq + '" \
												DemodMode="QAMAuto" \
												ProgramSelectionMode="PATProgram" \
												ProgramID="' + ch.program + '"> \
											</DigitalChannelParams> \
										</ChannelParams>'));
	} 
	else if (ch.type == "rf" && !isDigital) {
	    ChanList.push(BuildChannel('"' + ch.label + '"', 
                                        '<ChannelParams ChannelType="Analog" \
                                            ChannelUsage="' + ChanUsage + '"> \
                                            <AnalogChannelParams \
                                                PhysicalChannelIDType="Freq" \
                                                PhysicalChannelID="' + ch.freq + '"> \
                                            </AnalogChannelParams> \
                                        </ChannelParams>'));
	}
	else if (ch.type == "ip") {
		ChanList.push(BuildChannel('"' + ch.label + '"', 
										'<ChannelParams ChannelType="UDP" \
											Encryption="Proidiom" \
											ChannelUsage="' + ChanUsage + '"> \
											<UDPChannelParams \
												Address="' + ch.ip + '" \
												Port="' + ch.port + '"> \
											</UDPChannelParams> \
										</ChannelParams>'));
	}													
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

