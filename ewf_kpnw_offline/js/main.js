var Main = View.extend({

    id: 'main',
    
    //template: 'main.html',
    
    //css: 'main.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
        if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {
            //main page never closes
            return true;
        }
		else if(key == 'POWR') {
            return true;
        }
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
            
        // watch tv
        if( linkid == 'nav-watchtv') {
            this.watchTV(linkid);
            return true;
        }
        
        return false;
    },
    
    uninit: function() {
        $.doTimeout('main clock');    // stop clock
        $.doTimeout('main page poll');    // stop polling
    },
    
    renderData: function() {
        var content = loadTemplate(this.id);
        this.$el.html(content);
        this.$el.addClass('fullscreen');
        
        var context = this;
        $.doTimeout('main page poll', 10000, function() {
            context.checkStatus();
            return true;
        });
        
        // start clock
        $.doTimeout('main clock', 60000, function() {
            var d = new Date();
            var format = 'h:MM TT dddd, mmmm d, yyyy';
            var locale = window.settings.language;
            context.$('#datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('main clock', true); // do it now
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    checkStatus: function() {
    },
    
    watchTV: function(className) {
         var context = this;
         this.tvChannels = [];
         var channels = Chan_Table; // defined in chan_defs.js
         $.each(channels, function(i, ch) {
             context.tvChannels.push({type: 'Analog', url: ch.ID, display: ch.label});
         });
         var page = new VideoPlayer2({className:className, playlist: this.tvChannels, playlistIndex: 0, delay: 10000, delayMessage: 'One moment while we are loading the television channels...'});
         page.render();
     }
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
});    