var Careboard = View.extend({

    id: 'careboard',

    template: 'careboard.html',

    css: 'careboard.css',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function (key) {
        if (key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#buttons', 'a.button');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
        var ret = true;

        if ($jqobj.hasClass('back')) { // back button
            this.destroy();
            ret = true;
        }

        switch (linkid) {
            case 'notes':
                var page = new MyNotes({className: 'mynotes'});
                page.render();
                break;
            case 'painscale':
                var data = {
                    text1: '<p>We care about your comfort.</p><p>Please discuss your pain level with your clinical staff.</p>',
                    image1: '<div id="wongbaker"></div>'
                };
                var page = new Information({className: 'painscale', data: data, trackPageView: true});
                page.render();
                break;
            case 'mysupport':
                var page = new MySupport({className: 'mysupport'});
                page.render();
                break;
            default:
                ret = false;
                break;
        }

        return ret;
    },

    focus: function ($jqobj) {
        this._super($jqobj);
    },

    blur: function ($jqobj) {
        this._super($jqobj);
    },

    renderData: function () {
        var context = this;
        var data = this.data;

        // start clock
        $.doTimeout('careboard clock', 20000, function () {
            var d = new Date();
            var format = 'dddd, mmm d<br/>h:MM TT';
            var locale = window.settings.language;
            context.$('#basicinfo #today #time').html(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('careboard clock', true); // do it now

        // start data polling every 5 minutes.
        $.doTimeout('careboard data polling', 300000, function () {
            context._updateData();
            return true;
        });

        // return right away so the page shows 'loading' first.
        // we delay a bit before updating the real data
        // this is to prevent the UI blocking experience
        $.doTimeout(200, function () {
            context._updateData();
            return false;
        });
    },

    uninit: function () {
        $.doTimeout('careboard clock'); // stop clock
        $.doTimeout('careboard data polling');
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

    _updateData: function () {
        var ewf = ewfObject();
        // Get Patient MRN
        var mrn = this.mrn;
        if (!mrn)
            mrn = getMRNDATA();

        msg(mrn);
        this.mrn = mrn;

        var dataobj = {};
        dataobj['room'] = window.settings.room;
        dataobj['bed'] = window.settings.bed;
        dataobj['name'] = window.settings.userFullName;
        var preferredname = '';

        var main_url = ewf.getclinical + '?'; //"http://10.54.10.104:9080/ams/aceso/getClinicalData?"

        // Get Patient Preferred Name
        url = main_url + "type=patient&mrn=" + mrn + "&numrec=1&sortorder=asc"
        var dataobj = '';
        var xml = getXdXML(url, dataobj);

        $(xml).find("item").each(function () {
            preferredname = ($(this).find("value").text());
        });


        // Get Estimated Discharge Date
        url = main_url + "type=estimateddisdate&mrn=" + mrn + "&numrec=1&sortorder=asc"
        var estimateddisdate = '';
        var dataobj = '';
        var xml = getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            var ed = ($(this).find("value").text());
            if (ed != '') {
                estimateddisdate = new Date(ed.substr(0, 4) + "/" + ed.substr(4, 2) + "/" + ed.substr(6, 2))
                estimateddisdate = estimateddisdate.format("mm-dd-yyyy");
            }
        });


        // Get diet Preferred Name
        url = main_url + "type=diet&mrn=" + mrn + "&numrec=4&sortorder=asc"
        var dataobj = '';
        var dietdesc = '';
        var xml = getXdXML(url, dataobj);

        $(xml).find("item").each(function () {
            dietdesc = '<p>' + $(this).find("value").text() + '</p>';
        });


        // TODO Add code when dietary and support person data is ready
        var diet = '<div class="header">My Diet:</div>';
        if (dietdesc.length >= 1) {
            diet = diet + dietdesc;
        } else {
            diet = diet + '<p>Please discuss your Dietary needs with your care team</p>';
        }


        url = main_url + "type=allergies&mrn=" + mrn + "&numrec=10&sortorder=asc"
        var dataobj = '';
        var allergies = '';
        var xml = getXdXML(url, dataobj);
        cnt = 0;
        $(xml).find("item").each(function () {
            if (cnt != 0)
                allergies = allergies + ', ';
            allergies = allergies + $(this).find("value").text();
            cnt = cnt + 1;
        });
        allergies = allergies + '</p>';

        // TODO Add code when dietary and support person data is ready
        if (allergies.length >= 1) {
            diet = diet + '<br/>Food Allergies: ' + allergies;
        } else {
            diet = diet + '<br/>Food Allergies: Unknown';
        }


        var support = '<div class="header">My Support Person:</div>';
        support = support + '<p>Please enter your Support Person info on your bedside terminal</p>';


        // Get Careteam Data
        url = main_url + "type=careteam&mrn=" + mrn + "&numrec=200&sortorder=asc"
        var dataobj = '';
        var enddate = '';
        var careteam = '<div class="header">My Care Team:</div>';
        cnt = 0;
        var xml = getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            if (cnt <= 11) {
                desc = ($(this).find("codedescription").text());
                // decided not to show relationship
                //if (desc != 'Attending Physician')
                //	desc = ''
                value = ($(this).find("value").text());
                enddate = ($(this).find("enddate").text());
                if (enddate.length >= 1)
                    var edate = parseDateTime(enddate);
                msg(value);
                var now = Date();
                var datecompare = 0;
                if (enddate.length >= 1)
                    datecompare = compareDates(edate, now);
                if (datecompare >= 0) {
                    if (desc != '' && desc.length >= 1) {
                        careteam = careteam + '<p>' + value + ", " + desc + '</p>';
                    } else {
                        careteam = careteam + '<p>' + value + '</p>';
                    }
                    cnt = cnt + 1;
                }
            }
        });


        // Get Activity     
        url = main_url + "type=activity&mrn=" + mrn + "&numrec=6&sortorder=asc"
        var dataobj = '';
        var activity = '<div class="header">My Activity:</div>';
        var xml = getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            desc = ($(this).find("codedescription").text());
            value = ($(this).find("value").text());
            value = value.replace(/\;/g, '<br/>');
            value = value.replace(/\:/g, '<br/>');
            value = value.replace(/\,/g, '<br/>');
            code = ($(this).find("code").text());
            if (code == 'ACES-5937' && (value.length >= 1))
                value = "Staff Assist - " + value;
            activity = activity + '<p>' + value + '<br/></p>';
        });

        // Get Goals
        var url = "http://10.54.10.104:9080/ams/aceso/getClinicalData?"
        url = url + "type=goals&mrn=" + mrn + "&numrec=1&sortorder=asc"
        var dataobj = '';
        var goals = '<div class="header">My Goals:<span>See wall eCareBoard for more goals</span></div>';
        var xml = getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            desc = ($(this).find("codedescription").text());
            value = ($(this).find("value").text());
            value = value.replace(/\;/g, '<br/>');
            value = value.replace(/\:/g, '<br/>');
            value = value.replace(/\,/g, '<br/>');
            goals = goals + '<p>' + value + '</p>';
        });


        if (preferredname) {
            this.$('#basicinfo #name').text(window.settings.userFullName + '     "' + preferredname + '"');
        } else {
            this.$('#basicinfo #name').text(window.settings.userFullName);
        }


        this.$('#dischargedate').html('<div class="header2"><span>Estimated Discharge: </span>' + estimateddisdate + '</div>');

        this.$('#careteam').html(careteam);
        this.$('#activity').html(activity);
        this.$('#goals').html(goals);
        this.$('#diet').html(diet);
    }
});    