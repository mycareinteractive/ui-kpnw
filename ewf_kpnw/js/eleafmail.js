var eleafmail = View.extend({

    id: 'eleafmail',
    
    template: 'eleafmail.html',
    
    css: 'eleafmail.css',

    
	
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
        if(key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#selections');   
            return true;
        }
        if(key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#selection #body', '.submenu');
            return true;
        }
        return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {

		var linkid = $jqobj.attr('id');
		var type = $jqobj.attr('type');
        this.linkid = linkid;
		
		if(type == 'answer') {            
			this._renderResponse();
            return true;
        }
	
        if($jqobj.hasClass('back')) { // back button
			if(this.closeall==true) {			
				keypressed('CLOSEALL');
				keypressed(216);    //force going back to main menu
			}
				
    		this.destroy();
    		return true;
    	} 
		if($jqobj.hasClass('menu')) {		
			this.currsel = this.$('#selections a.active');
		}
    	return false;
    },
    
    focus: function($jqobj) {
        this._super($jqobj);
        if(!$jqobj.hasClass('back')) {
            $('#eleafmail #background').addClass('textoverlay');
            this._renderSubData($jqobj);
        }
    },
    
    blur: function($jqobj) {
        this._super($jqobj);
        this.$('#selection #body').html('');
    },
    
    renderData: function() {

		var context = this;
		var url = './survey.xml';
        var xml = getXML(url);
		this.xmlsub = xml;
		
		var surveyname = ""
		var surveyid = ""
          $(xml).find("Survey").each(function() {                   			
             surveyname = $(this).attr("Name");
			surveyid = $(this).attr("id");
          });
			
        var c = $('<p></p>').html(surveyname);
        this.$('#label').append(c);
          $(xml).find("Question").each(function() {                   								
				    var question = $(this).attr("Title");
					var id = $(this).attr("ID");
					var text = $(this).attr("Text");					
					var o = $('<a href="#"></a>').html(question).attr('id','m'+id).attr('title',question).addClass('menu ' + id);                                                   					
		            context.$('#selections').append(o);   														
          });
				
	    this.$('#selections').append('<a class="back" href="#" title="back" data-translate="back">Back</a>');		
        var $firstObj = this.$('#selections a:first');
        this.focus($firstObj);
		this.currsel = this.$('#selections a.active');
		this.surveyname = surveyname;
		this.surveyid = surveyid;

    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Privatfe functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
	_renderResponse: function() {

		var context = this;		
		var currsel = this.currsel;
		var surveyid = this.surveyid;
		var surveyname = this.surveyname;
		var curranswerid = this.linkid;
		var currid = currsel.attr("id");
		
		var xmlsub = this.xmlsub;
        var ewf = ewfObject();
		var room = window.settings.room;
		var bed = window.settings.bed;
		
        // Get Patient MRN
		var mrn = this.mrn;
		if(!mrn)
			mrn = getMRNDATA();
							
	
		$(xmlsub).find("Question").each(function() {                   					
			var questionid = $(this).attr("ID");
			var quesid  = 'm'+questionid;
			var questiontext = $(this).attr("Text");			
			
			if(quesid==currid) {				
				var xmlsub2 = $(this);
				var o = "";
				$(xmlsub2).find("Answer").each(function() {                   												    
					var answerid = $(this).attr("id");		
					var ansid = 'm'+answerid;
					var answertext = $(this).attr("text");	
					var answervalue = $(this).attr("value");
					
					if(curranswerid==ansid) {
						var xmlsub3 = $(this);
						$(xmlsub3).find("Response").each(function() {                   												    
							var responseid = $(this).attr("id");
							var responsetext = $(this).attr("text");
							context.$("#selection #body").html('<p class="submenu active">' + responsetext + '</p>');												
													
							//* Handle response

							var xmlsub4 = $(this);
							var main_url = "http://10.54.10.104:9980/Surveys"						                
							var dataobj = "";
							var resultnotifid = "";
							var resultstatus = "";
							$(xmlsub4).find("Notification").each(function() {          
								dataobj = {};
								dataobj['mrn'] = mrn;
								dataobj['uri'] =$(this).attr("uri");
								dataobj['room'] = room;
								dataobj['bed'] = bed;
								dataobj['param'] = $(this).attr("param");
								dataobj['subject'] =$(this).attr("subject");
								dataobj['body'] = $(this).attr("body");
								dataobj['payload'] = $(this).attr("payload");
								var subject = $(this).attr("subject");
								msg(subject);
								msg(subject.indexOf("%servicegroup%") );
								msg('subject ' + subject);
								var xml  = getXdXML(main_url, dataobj);      
								
								resultnotifid = resultnotifid + $(this).attr("id") +";";
								resultstatus = resultstatus +  $(this).attr("type") +";";
								
							});

							//*Save Results
							var result_url = "http://10.54.10.104:9985/Results"						                
							var resultdataobj = "";
							resultdataobj = {};
							resultdataobj['mrn'] = mrn;
							resultdataobj['roombed'] = room + "_" + bed;															
							resultdataobj['surveyid'] = surveyid;
							resultdataobj['surveyname'] = surveyname;
							resultdataobj['questionid'] = questionid;
							resultdataobj['questiontext'] = questiontext;							
							resultdataobj['answerid'] = answerid;							
							resultdataobj['answertext'] = answertext;
							resultdataobj['answervalue'] = answervalue;														
							resultdataobj['responseid'] = responseid;							
							resultdataobj['responsetext'] = responsetext;														
							resultdataobj['resultnotifid'] = resultnotifid;							
							resultdataobj['resultstatus'] = resultstatus;							
														
							var xml  = getXdXML(result_url, resultdataobj);      

						});

					}					
				});
						
			}
        });		         
        return true;
	
	},

	
    _renderSubData: function() {
	
		var currsel = this.$('#selections a.active');       		
		var currid = currsel.attr("id");
		var xmlsub = this.xmlsub;
		var context = this;
         $(xmlsub).find("Question").each(function() {                   					
			var id = 'm'+$(this).attr("ID");
			var text = $(this).attr("Text");
					
			if(id==currid) {
				context.$("#selection #body").html('<p class="submenu active">' + text  +'<br><br></p>');
				var xmlsub2 = $(this);
				var o = "";
				$(xmlsub2).find("Answer").each(function() {                   												    
					var id = $(this).attr("id");
					var text = $(this).attr("text");		
					
					var o = $('<a href="#"></a>').html(text).attr('id','m'+id).attr('type','answer').addClass('button');                                                   					
					context.$('#selection #body p.submenu').append(o);   														
					context.$('#selection #body p.submenu').append('&nbsp;&nbsp;&nbsp;');
					
				});
						
			}
       });	
			
    },
    
    
});    